// importar la libreria asociada para leer los archivos de excel y pasarlos a json
// para instalar esta carpeta es necesario ejecutar el comando npm install xlsx
// posterior aparecera en la carpeta del programa package-lock.json
//URl del Video https://www.youtube.com/watch?v=tKz_ryychBY

//Para mas infor consultar la pag https://docs.sheetjs.com
 var xlsx = require("xlsx");
//Para leer de forma adecuada los formatos de fechas es necesario mediante 
 var wb = xlsx.readFile("Información Personal.xlsx",{cellDates:true});

 //para seleccionar una pagina
 var impuestos = wb.Sheets["Impuestos"];

//  para convertir esa información en un array mediante json

var data = xlsx.utils.sheet_to_json(impuestos);

var newdata = data.map(function(record){
    record.net = record.Debe - record.Pago ;
    delete record.Debe;
    delete record.Pago;
    return record;
});
 console.log(newdata);